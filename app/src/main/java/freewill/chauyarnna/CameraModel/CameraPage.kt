package freewill.chauyarnna.CameraModel

import android.os.Bundle
import freewill.chauyarnna.R
import android.content.Context
import android.view.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_camera_page.view.*
import java.lang.ClassCastException


class CameraPage() : Fragment(){
    private var onTake:CameraInterface? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.activity_camera_page, container, false)
        view.capture_button.setOnClickListener {
           onTake!!.ClickCheck(1)
           view.gallery_camera.setImageBitmap(onTake!!.setBitmap())
        }



        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
         onTake = context as CameraInterface

        }catch (e:ClassCastException){
            throw ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    override fun onDetach() {
        super.onDetach()
        onTake = null
    }

}
