package freewill.chauyarnna.CameraModel

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import freewill.chauyarnna.OCRModel.OCR_tts
import freewill.chauyarnna.R
import kotlinx.android.synthetic.main.activity_camera_quick_page.view.*
import java.lang.ClassCastException

class CameraQuickPage : Fragment() {
    private var onTake:CameraInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.activity_camera_quick_page, container, false)
        view.capture_button_quick.setOnClickListener {
            onTake!!.ClickCheck(2)
            val ocr = OCR_tts()
            ocr.detectText(onTake!!.setBitmap())
        }
        return view
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            onTake = context as CameraInterface

        }catch (e: ClassCastException){
            throw ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    override fun onDetach() {
        super.onDetach()
        onTake = null
    }

}
