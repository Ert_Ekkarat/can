package freewill.chauyarnna

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager

class MainActivity : AppCompatActivity() {

    private lateinit var fragment: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragment = supportFragmentManager


        val transaction = fragment.beginTransaction()
        transaction.replace(R.id.container_frame,homePage())
        transaction.addToBackStack(null);
        transaction.commit()
    }
}
