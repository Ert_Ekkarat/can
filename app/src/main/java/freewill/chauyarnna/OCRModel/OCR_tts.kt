package freewill.chauyarnna.OCRModel

import android.app.Activity
import android.content.Context
import android.content.Context.CAMERA_SERVICE
import android.graphics.Bitmap
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.os.Build
import android.util.Log
import android.util.SparseIntArray
import android.view.Surface
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.text.FirebaseVisionText

class OCR_tts{
 private val TAG:String = OCR_tts::class.java.simpleName


  fun detectText(bitmap:Bitmap){
    val image = FirebaseVisionImage.fromBitmap(bitmap)
    val detector = FirebaseVision.getInstance()
          .onDeviceTextRecognizer
    val result = detector.processImage(image)
          .addOnSuccessListener { firebaseVisionText ->
                processText(firebaseVisionText)
          }
          .addOnFailureListener {

          }
  }
  fun processText(firebaseVisionText: FirebaseVisionText){
      val resultText =firebaseVisionText.text
      Log.d(TAG,resultText)
      for (block in firebaseVisionText.textBlocks) {
          val blockText = block.text


      }
  }
}
