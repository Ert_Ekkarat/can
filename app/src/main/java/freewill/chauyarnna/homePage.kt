package freewill.chauyarnna

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import freewill.chauyarnna.AudioModel.AduioNonsavePage
import freewill.chauyarnna.CameraModel.MainCamera
import kotlinx.android.synthetic.main.activity_home_page.*
import kotlinx.android.synthetic.main.activity_home_page.view.*

class homePage : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.activity_main, container, false)

        view.main_read_ocr.setOnClickListener {
            val intent = Intent(activity,MainCamera::class.java)
            startActivity(intent)
        }

        return view;
    }

}
